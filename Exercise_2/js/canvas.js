var newcanvas;
var arr = [];
function generateCanvas() {
  //Random canvas generation and creating the dropdown of canvas Id
  var randomNum = Math.round(Math.random() * 10);
  if (randomNum > 5) {
    randomNum = randomNum - 5;
  }
  if (randomNum < 3) {
    randomNum = randomNum + 3;
  }
  if (randomNum > 2 && randomNum <= 5) {
    console.log(randomNum);
    for (var i = 0; i < randomNum; i++) {
      var canvas = document.createElement("canvas");
      select = document.getElementById("canvasID");
      canvas.id = "canvas" + i;
      var canvasId = canvas.id;
      canvas.height = "200";
      canvas.width = "200";
      canvas.style.border = "1px solid black";
      canvasDiv.appendChild(canvas);
      newcanvas = window._canvas = new fabric.Canvas(canvasId);
      arr.push(newcanvas);
      newcanvas.__onMouseDown = mouseDown;
      newcanvas.__onMouseUp = mouseUp;
      ele = document.createElement("option");
      text = document.createTextNode(canvasId);
      ele.appendChild(text);
      att = document.createAttribute("value");
      att.value = canvasId;
      ele.setAttributeNode(att);
      select.appendChild(ele);

    }
    document.getElementById("generateShapes").removeAttribute('disabled')
  }

}

//generating the different  shapes in the selected canvas
function generateShapes() {
  var getID = document.getElementById("canvasID").selectedOptions[0].value;
  var getShape = document.getElementById("shapes").selectedOptions[0].value;
  var selectedcanvas = arr[parseInt(getID.charAt(getID.length - 1))]
  var uc = document.getElementsByClassName('upper-canvas');
  var ele = uc[(parseInt(getID.charAt(getID.length - 1)) + 1)]
  if (ele && ele.previousElementSibling.className !== "lower-canvas") {
    ele.remove();
  }
  var sqaure = new fabric.Rect({ top: 50, left: 50, width: 30, height: 30, fill: 'red' });
  var rect = new fabric.Rect({ top: 50, left: 100, width: 60, height: 30, fill: 'blue' });
  var circle = new fabric.Circle({ top: 100, left: 50, radius: 15, fill: 'green' });
  var triangle = new fabric.Triangle({ top: 100, left: 100, width: 50, height: 50, fill: 'yellow' })

  switch (getShape) {
    case "Square":
      selectedcanvas.add(sqaure).renderAll().setActiveObject(sqaure);
      break;
    case "Rectangle":
      selectedcanvas.add(rect).renderAll().setActiveObject(rect);
      break;
    case "Triangle":
      selectedcanvas.add(triangle).renderAll().setActiveObject(triangle);
      break;
    case "Circle":
      selectedcanvas.add(circle).renderAll().setActiveObject(circle);
      break;
  }

}
var activeObject, initialCanvas;
function mouseDown(event) {
  if (this.getActiveObject()) {
    activeObject = $.extend({}, this.getActiveObject());
    initialCanvas = this.lowerCanvasEl.id;
  }
}

function mouseUp(event) {
  if (event.target.localName === 'canvas' && initialCanvas) {
    canvasId = $(event.target).siblings().attr('id');
    if (canvasId !== initialCanvas) {
      arr[canvasId.charAt(canvasId.length - 1)].add(activeObject);
      arr[canvasId.charAt(canvasId.length - 1)].renderAll();
    }
  }
  initialCanvas = '';
  activeObject = {};
}
