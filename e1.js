
var url = "https://jsonplaceholder.typicode.com/todos";
var jsonData;
var tableData = [];
function loadJSON(url, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success) success(JSON.parse(xhr.responseText));
            } else {
                if (error) error(xhr);
            }
        }
    };
    xhr.open("GET", url, true);
    xhr.send();
}

loadJSON(
    url,
    function (data) {
        jsonData = data;
        var userIdArray = [];
        //-----------------drop drown creation-----------------
        for (var i = 0; i < jsonData.length; i++) {
            if (userIdArray.indexOf(jsonData[i].userId) == -1) {
                userIdArray.push(jsonData[i].userId);
            }
        }

        var element = document.getElementById("userId");
        for (var o = 0; o < userIdArray.length; o++) {
            var option = document.createElement("option");
            option.setAttribute("value", userIdArray[o]);
            option.setAttribute("id", "user");
            option.innerHTML = userIdArray[o];
            element.appendChild(option);
        }
    },
    function (xhr) {
        console.error(xhr);
    }
);
var falseCount, trueCount;


// ------------DropDown onchange functionality ----------
function userIdClick(event) {
    var changedValue = document.getElementById("userId").value;
    console.log(changedValue);
    tableData = [];
    for (var i = 0; i < jsonData.length; i++) {
        if (jsonData[i].userId == changedValue) {
            tableData.push(jsonData[i]);
        }
    }

    tableCreation(tableData);
}
// ------------Table Creation starts----------
function tableCreation(tableData) {
    var table = document.getElementById("todoTable");
    document.getElementById("todoHeading").style.display = 'block';
    document.getElementById("canvasDiv").style.display = 'block';
    while (table.firstChild) {
        table.removeChild(table.firstChild);
    }
    trueCount = 0;
    if (tableData.length > 0) {
        for (var o = 0; o < tableData.length; o++) {
            var row = document.createElement("tr");
            var colID = document.createElement("td");
            colID.innerHTML = tableData[o].id;
            var colTitle = document.createElement("td");
            colTitle.innerHTML = tableData[o].title;
            var colComplete = document.createElement("td");
            var check = document.createElement("input");
            check.setAttribute("disabled", "");
            if (tableData[o].completed) {
                check.setAttribute("checked", "");
                trueCount = trueCount + 1;
            }

            colComplete.appendChild(check);
            check.type = "checkbox";
            row.appendChild(colID);
            row.appendChild(colTitle);
            row.appendChild(colComplete);
            table.appendChild(row);
            // ------------Table Creation ends----------

        }
    }
    else {
        var row = document.createElement("tr");
        row.innerHTML = "No Data to Display";
        table.appendChild(row);
    }
    falseCount = (tableData.length - trueCount) * 100 / tableData.length;
    trueCount = trueCount * 100 / tableData.length;
    var itemValue = [trueCount, falseCount];
    init(itemValue);
}


// ------------Canvas Creation starts----------
var canvas;
var context;
var Val_Max;
var Val_Min;
var sections;
var xScale;
var yScale;
var y;
var itemName = ["", ""];
function init(itemValue) {
    sections = 5;
    Val_Max = 100;
    var stepSize = 10;
    var columnSize = 50;
    var rowSize = 60;
    var margin = 10;
    var header = "Completion rate %";
    var oldcanv = document.getElementById('canvas');
    var canvasDiv = document.getElementById('canvasDiv');
    if (canvasDiv.childElementCount > 0) {
        canvasDiv.removeChild(oldcanv)
    }

    var canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    canvas.height = "400";
    canvas.width = "650";
    canvasDiv.appendChild(canvas);
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    context.fillStyle = "#000;";
    yScale = (canvas.height - columnSize - margin) / Val_Max;
    xScale = (canvas.width - rowSize) / (sections + 1);

    context.strokeStyle = "#000;";
    context.beginPath();
    context.fillText(header, 0, columnSize - margin);
    var count = 0;
    for (scale = Val_Max; scale >= 0; scale = scale - stepSize) {
        y = columnSize + yScale * count * stepSize;
        context.fillText(scale, margin, y + margin);
        context.moveTo(rowSize, y);
        context.lineTo(canvas.width, y);
        count++;
    }
    context.stroke();
    context.font = "20 pt Verdana";
    context.textBaseline = "bottom";
    for (i = 0; i < itemValue.length; i++) {
        computeHeight(itemValue[i]);
        context.fillText(itemName[i], xScale * (i + 1), y - margin);
        context.fillStyle = "#41f49a";
    }
    context.fillStyle = "#9933FF;";
    context.shadowColor = "rgba(128,128,128, 0.5)";
    context.shadowOffsetX = 9;
    context.shadowOffsetY = 3;
    context.translate(0, canvas.height - margin);
    context.scale(xScale, -1 * yScale);
    for (i = 0; i < itemValue.length; i++) {
        context.fillRect(i + 1, 0, 0.3, itemValue[i]);
        context.fillStyle = "#f46b41";

    }
    function computeHeight(value) {
        y = canvas.height - value * yScale;
    }
}

function tableSort(e) {
    if (e.target.innerHTML == "Todo ID ⇅") {
        if (tableData[tableData.length - 1].id > tableData[0].id) {
            tableData.sort(function (a, b) {
                return b.id - a.id;
            });
        }
        else {
            tableData.sort(function (a, b) {
                return a.id - b.id;
            });
        }
    }
    else if (e.target.innerHTML == "Todo Title ⇅") {
        if (tableData[tableData.length - 1].title < tableData[0].title) {
            tableData.sort(function (a, b) {
                var nameA = a.title.toLowerCase(),
                    nameB = b.title.toLowerCase();
                if (nameA < nameB) { return -1 }
                if (nameA > nameB) { return 1 }
                return 0
            });
        }
        else {
            tableData.sort(function (a, b) {
                var nameA = a.title.toLowerCase(),
                    nameB = b.title.toLowerCase();
                if (nameA > nameB) { return -1 }
                if (nameA < nameB) { return 1 }
                return 0
            });
        }
    }

    else if (e.target.innerHTML == "Completed? ⇅") {
        if (tableData[0].completed) {
            tableData.sort(function (a, b) {
                if (a.completed < b.completed) { return -1 }
                if (a.completed > b.completed) { return 1 }
                return 0
            })
        }
        else {
            tableData.sort(function (a, b) {
                if (a.completed > b.completed) { return -1 }
                if (a.completed < b.completed) { return 1 }
                return 0
            })
        }
    }
    tableCreation(tableData);
}